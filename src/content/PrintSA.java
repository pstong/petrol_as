/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.Anchor;
import static com.itextpdf.text.Annotation.FILE;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import static com.itextpdf.text.Element.ALIGN_CENTER;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.lowagie.text.Cell;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.field.RtfPageNumber;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;

public class PrintSA extends PdfPageEventHelper{
     private static String INPUTFILE = "NEARPOINT.pdf";
    private static String OUTPUTFILE="ReadPdf.pdf";
    
    private static Font nearpointFont = new Font(Font.FontFamily.TIMES_ROMAN, 11,
            Font.BOLD);
    private static Font addressFont = new Font(Font.FontFamily.TIMES_ROMAN, 11,
            Font.NORMAL);
    private static Font SAFont = new Font(Font.FontFamily.TIMES_ROMAN, 11,
            Font.UNDERLINE);
    private static Font tableFont = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL);
    
    private static Font footer=new Font(Font.FontFamily.TIMES_ROMAN,7,Font.ITALIC);
    
    
    
    public static void main(String[] args) throws FileNotFoundException, DocumentException, com.lowagie.text.BadElementException{
        Document document = new Document(PageSize.A4);
        
        PdfWriter writer=PdfWriter.getInstance(document, new FileOutputStream(INPUTFILE));
        PrintSA event=new PrintSA();
        writer.setPageEvent(event);
        document.open();
        document.setMargins(5, 5, 5, 5);
        add_header_company(document);
        add_footer(writer,document);
        document.close();
        
        
        
    }

   
    
    private static void add_header_company(Document document) throws DocumentException{
        
        Paragraph p1=new Paragraph("NEARPOINT (M) SDN BHD", nearpointFont);
        Paragraph anchor1=new Paragraph("STATION MINYAK SHELL", addressFont);
        Paragraph anchor2=new Paragraph("BATU 5, JALAN TAMPIN LAMA,", addressFont);
        Paragraph anchor3=new Paragraph("70450 SEREMBAN, N.S.D.K.", addressFont);
        Paragraph anchor4=new Paragraph("GST NO: 001524998144", addressFont);
        Paragraph anchor5=new Paragraph("TEL: 06-6771877", addressFont);
        Paragraph anchor6=new Paragraph("STATEMENT OF ACCOUNT", SAFont);
        p1.setAlignment(ALIGN_CENTER);
        anchor1.setAlignment(ALIGN_CENTER);
        anchor2.setAlignment(ALIGN_CENTER);
        anchor3.setAlignment(ALIGN_CENTER);
        anchor4.setAlignment(ALIGN_CENTER);
        anchor5.setAlignment(ALIGN_CENTER);
        anchor6.setAlignment(ALIGN_CENTER);
        
        document.add(p1);
        document.add(anchor1);
        document.add(anchor2);
        document.add(anchor3);
        document.add(anchor4);
        document.add(anchor5);
        document.add(anchor6);
    }
    
 
    private static void add_footer(PdfWriter writer, Document document) throws com.lowagie.text.BadElementException{
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("Should you fail to challenge the authenticity of this Statement of "
                + "Account within 30 days from this date, it will be deemed that you ",footer), 50, 40, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("agree to the said statement and your chits for purchase made shall be destroyed for want of space." ,footer), 50, 30, 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase("No payment will be recognised unless supported by our Official Receipt." ,footer), 50, 10, 0);
         
         
         
         
        
    }
    
    private static void add_customer_detail(Document document){
       
        
    }
    
    private static void add_content(Document document) throws DocumentException{
       
        
    }
    
     private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
