/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Jian Wei Long
 */
public class Vehicle {
    File file=new File("vehicle.xls");
    
    public void create_vehicle_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("vehicle.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean add_vehicle(String[] vehicleInfo)throws WriteException, IOException, BiffException{
        boolean addVehicle = false;
        Workbook aWorkBook = Workbook.getWorkbook(new File("customer.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("customer.xls"), aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        int isAccExist = 0;
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            String cell2 = sheet.getCell(1, i).getContents();
            if(cell.equals(vehicleInfo[0]) || cell2.equals(vehicleInfo[1])){
                isAccExist++;
                break;
            }
        }        
        aCopy.write();
        aCopy.close();
        
        if(isAccExist != 0){
            Workbook bWorkBook = Workbook.getWorkbook(file);
            WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
            WritableSheet bCopySheet = bCopy.getSheet(0);
            
            int row = bCopySheet.getRows();
            for(int i=0; i<vehicleInfo.length; i++){
                bCopySheet.addCell(new Label(i, row, vehicleInfo[i]));
            }

            bCopy.write();
            bCopy.close();
            
            addVehicle = true;
        }
        return addVehicle;
    }
    
    public void delete_vehicle(String data)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                aCopySheet.removeRow(i);
                break;
            }
        }
                
        aCopy.write();
        aCopy.close();
    }
    
    public boolean edit_vehicle(String[] vehicleInfo) throws IOException, BiffException, WriteException{
        boolean editVehicle = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            String cell2 = sheet.getCell(2, i).getContents();
            if(cell.equals(vehicleInfo[0]) && cell2.equals(vehicleInfo[1])){
                aCopySheet.addCell(new Label(2, i, vehicleInfo[2]));
                aCopySheet.addCell(new Label(3, i, vehicleInfo[3]));
                editVehicle = true;
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        update_vehicle(vehicleInfo);
        
        return editVehicle;
    }
    
    public Vector view_vehicle_data(){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                data.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_vehicle_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Account No 1");
        headers.add("Account No 2");
        headers.add("Vehicle No");
        headers.add("Car Model");
        
        return headers;
    }
    
    public String[] get_vehicle(String accno){
        Vector<String> vehicle = new Vector<String>();
        if(file.exists()){
            try {
                Workbook workbook = Workbook.getWorkbook(file);
                Sheet sheet = workbook.getSheet(0);
                
                for (int j = 0; j < sheet.getRows(); j++) {
                    String cell = sheet.getCell(0, j).getContents();
                    if(cell.equals(accno)){
                        vehicle.add(sheet.getCell(2, j).getContents());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(vehicle.isEmpty()){
            String[] empty = {"No Vehicle."};
            return empty;
        }
        
        String[] vehicle_array = vehicle.toArray(new String[vehicle.size()]);
        return vehicle_array;
    }
    
    public void update_vehicle(String[] vehicleInfo) throws IOException, BiffException, WriteException{
        //update chit
        Workbook aWorkBook = Workbook.getWorkbook(new File("chit.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("chit.xls"), aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String acc = sheet.getCell(4, i).getContents();
            String vec = sheet.getCell(6, i).getContents();
            if(acc.equals(vehicleInfo[0]) && vec.equals(vehicleInfo[1])){
                aCopySheet.addCell(new Label(6, i, vehicleInfo[2]));
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        //update receipt
        Workbook bWorkBook = Workbook.getWorkbook(new File("receipt.xls"));
        WritableWorkbook bCopy = Workbook.createWorkbook(new File("receipt.xls"), bWorkBook);
        WritableSheet bCopySheet = bCopy.getSheet(0);
        Sheet bsheet = bCopy.getSheet(0);

        int btotal = bsheet.getRows();
        for(int i=0; i<btotal; i++){
            String acc = bsheet.getCell(2, i).getContents();
            String vec = bsheet.getCell(5, i).getContents();
            if(acc.equals(vehicleInfo[0]) && vec.equals(vehicleInfo[1])){
                bCopySheet.addCell(new Label(5, i, vehicleInfo[2]));
                break;
            }
        }
        
        bCopy.write();
        bCopy.close();
        
        //update statement
        /*
        Workbook cWorkBook = Workbook.getWorkbook(new File("statement_of_account.xls"));
        WritableWorkbook cCopy = Workbook.createWorkbook(new File("statement_of_account.xls"), cWorkBook);
        WritableSheet cCopySheet = cCopy.getSheet(0);
        Sheet csheet = cCopy.getSheet(0);

        int ctotal = csheet.getRows();
        for(int i=0; i<ctotal; i++){
            String acc = csheet.getCell(2, i).getContents();
            String vec = csheet.getCell(3, i).getContents();
            if(acc.equals(vehicleInfo[0]) && vec.equals(vehicleInfo[1])){
                cCopySheet.addCell(new Label(3, i, vehicleInfo[2]));
                break;
            }
        }
        
        cCopy.write();
        cCopy.close();
        */
    }
}
