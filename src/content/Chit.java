/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Jian Wei Long
 */
public class Chit {
    File file=new File("chit.xls");
    StatementAccount st = new StatementAccount();
    DateFormat dateForm = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    
    public void create_chit_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("chit.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean add_chit(String[] chitInfo)throws WriteException, IOException, BiffException{
        boolean addchit = false;
        Workbook aWorkBook = Workbook.getWorkbook(new File("customer.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("customer.xls"), aWorkBook);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        int isAccExist = 0;
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(chitInfo[4])){
                isAccExist++;
                break;
            }
        }        
        aCopy.write();
        aCopy.close();
        
        if(isAccExist != 0){
            //add into chit
            Workbook bWorkBook = Workbook.getWorkbook(file);
            WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
            WritableSheet bCopySheet = bCopy.getSheet(0);
            
            int row = bCopySheet.getRows();
            for(int i=0; i<chitInfo.length; i++){
                bCopySheet.addCell(new Label(i, row, chitInfo[i]));
            }
            //bCopySheet.addCell(new Label(chitInfo.length, row, "-"));
            
            bCopy.write();
            bCopy.close();
            
            //add into statement of account
            if(!st.file.exists()){
                st.create_statement_excel();
            }
            Workbook cWorkBook = Workbook.getWorkbook(st.file);
            WritableWorkbook cCopy = Workbook.createWorkbook(st.file, cWorkBook);
            WritableSheet cCopySheet = cCopy.getSheet(0);
            
            int row2 = cCopySheet.getRows();
            int checkcount = -1;
            String closing = "";
            DecimalFormat df2 = new DecimalFormat( "0.00" );
            if(row2 != 0){
                for(int i=0; i<row2; i++){
                    String accno = cCopySheet.getCell(2, i).getContents();
                    closing = cCopySheet.getCell(0, i).getContents();
                    if(accno.equals(chitInfo[4]) && "-".equals(closing)){
                        checkcount = i;
                    }
                }
                
                //get previos balance
                double pre_credit = 0.00;
                double pre_debit = 0.00;
                int get_row = -1;
                for(int j = 0; j<cCopySheet.getRows(); j++){
                    if(!cCopySheet.getCell(0, j).getContents().equals("-") && cCopySheet.getCell(2, j).getContents().equals(chitInfo[4])){
                        get_row = j;
                    }else if(cCopySheet.getCell(0, j).getContents().equals("-") && cCopySheet.getCell(2, j).getContents().equals(chitInfo[4])){
                        break;
                    }
                }
                if(get_row >=0){
                    Double balance = Double.parseDouble(cCopySheet.getCell(6, get_row).getContents());
                    if(balance > 0.00){
                        pre_credit = balance;
                    }else if(balance < 0.00){
                        String new_balance = String.valueOf(balance).replace("-", "");
                        pre_debit = Double.parseDouble(new_balance);
                    }
                }
                
                if(checkcount >= 0){
                    double debit = st.get_debit(chitInfo[4], chitInfo[7]) + pre_debit;
                    //double debit = Double.parseDouble(cCopySheet.getCell(4, checkcount).getContents()) + pre_debit;
                    //debit = debit + Double.parseDouble(chitInfo[5]);
                    double credit = st.get_credit(chitInfo[4], chitInfo[7]) + pre_credit;
                    double balance = credit - debit;
                    cCopySheet.addCell(new Label(0, checkcount, "-"));
                    cCopySheet.addCell(new Label(1, checkcount, "-"));
                    cCopySheet.addCell(new Label(2, checkcount, chitInfo[4]));
                    cCopySheet.addCell(new Label(3, checkcount, chitInfo[6]));
                    cCopySheet.addCell(new Label(4, checkcount, String.valueOf(df2.format(debit))));
                    cCopySheet.addCell(new Label(5, checkcount, String.valueOf(df2.format(credit))));
                    cCopySheet.addCell(new Label(6, checkcount, String.valueOf(df2.format(balance))));
                    cCopySheet.addCell(new Label(7, checkcount, dateForm.format(date)));
                }else{
                    double debit = st.get_debit(chitInfo[4], chitInfo[7]) + pre_debit;
                    //double debit = pre_debit;
                    //debit = debit + Double.parseDouble(chitInfo[5]);
                    double credit = pre_credit;
                    double balance = credit - debit;
                    cCopySheet.addCell(new Label(0, row2, "-"));
                    cCopySheet.addCell(new Label(1, row2, "-"));
                    cCopySheet.addCell(new Label(2, row2, chitInfo[4]));
                    cCopySheet.addCell(new Label(3, row2, chitInfo[6]));
                    cCopySheet.addCell(new Label(4, row2, String.valueOf(df2.format(debit))));
                    cCopySheet.addCell(new Label(5, row2, String.valueOf(df2.format(credit))));
                    cCopySheet.addCell(new Label(6, row2, String.valueOf(df2.format(balance))));
                    cCopySheet.addCell(new Label(7, row2, dateForm.format(date)));
                }
            }else{
                cCopySheet.addCell(new Label(0, 0, "-"));
                cCopySheet.addCell(new Label(1, 0, "-"));
                cCopySheet.addCell(new Label(2, 0, chitInfo[4]));
                cCopySheet.addCell(new Label(3, 0, chitInfo[6]));
                cCopySheet.addCell(new Label(4, 0, chitInfo[5]));
                cCopySheet.addCell(new Label(5, 0, "0.00"));
                cCopySheet.addCell(new Label(6, 0, "-"+chitInfo[5]));
                cCopySheet.addCell(new Label(7, 0, dateForm.format(date)));
            }
            
            cCopy.write();
            cCopy.close();
            
            addchit = true;
        }
        return addchit;
    }
    
    public void delete_chit(String data)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);
        Vector<String> chitInfo = new Vector<String>();
        int total = sheet.getRows();
        
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                for(int j =0; j<sheet.getColumns(); j++){
                    chitInfo.add(aCopySheet.getCell(j ,i).getContents());
                }
                //break;
            }
        }    
        
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                aCopySheet.removeRow(i);
                break;
            }
        }
                
        aCopy.write();
        aCopy.close();
        String[] chitArray = chitInfo.toArray(new String[chitInfo.size()]);

        //st.update_debit(chitArray);
        st.update_account(chitArray[4], "debit");
        st.update_account(chitArray[4], "credit");
    }
    
    public Vector view_chit_data(){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns()-1; i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                data.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_chit_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Chit No");
        headers.add("Chit Date");
        headers.add("Product Description");
        headers.add("Product Littres");
        headers.add("Account No");
        headers.add("Amount (RM)");
        headers.add("Vehicle No");
        
        return headers;
    }
    
    public String[] get_chitNo(){
        Vector<String> data = new Vector<String>();
        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            for (int j = 0; j < sheet.getRows(); j++) {
                Cell cell = sheet.getCell(0, j);
                data.add(cell.getContents());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(data.isEmpty()){
            String[] empty={"No Chit"};
            return empty;
        }
        String[] chitArray = data.toArray(new String[data.size()]);
        return chitArray;
    }
    
    public String[] load_chit(String chitno) throws IOException, BiffException, WriteException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        
        String[] result = new String[7];
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(chitno)){
                for (int j = 0; j < sheet.getColumns()-1; j++) {
                    result[j] = sheet.getCell(j, i).getContents();
                }
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return result;
    }
    
    public boolean edit_chit(String[] chitInfo) throws IOException, BiffException, WriteException{
        boolean editchit = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        String[] latestchitInfo = new String[8];
        int row = aCopySheet.getRows();
        for(int j=0; j<row; j++){
            String chitno = aCopySheet.getCell(0, j).getContents();
            if(chitno.equals(chitInfo[0])){
                for(int i=0; i<chitInfo.length; i++){
                    aCopySheet.addCell(new Label(i, j, chitInfo[i]));
                    latestchitInfo[i] = chitInfo[i];
                }
                latestchitInfo[7] = aCopySheet.getCell(7, j).getContents();
            }
        }
            
        aCopy.write();
        aCopy.close();
        //st.update_debit(latestchitInfo);
        st.update_account(latestchitInfo[4], "debit");
        st.update_account(latestchitInfo[4], "credit");
        editchit = true;
        
        return editchit;
    }
}
