/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author PC
 */
public class Receipt {
    File file=new File("receipt.xls");
    StatementAccount st = new StatementAccount();
    DateFormat dateForm = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    
    public void create_receipt_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("receipt.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean add_receipt(String[] receiptInfo)throws WriteException, IOException, BiffException{
        //add into receipt
        boolean addreceipt = false;
        Workbook aWorkBook = Workbook.getWorkbook(new File("customer.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("customer.xls"), aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        int isAccExist = 0;
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(receiptInfo[2])){
                isAccExist++;
                break;
            }
        }        
        aCopy.write();
        aCopy.close();
        
        if(isAccExist != 0){
            Workbook bWorkBook = Workbook.getWorkbook(file);
            WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
            WritableSheet bCopySheet = bCopy.getSheet(0);
            
            int row = bCopySheet.getRows();
            for(int i=0; i<receiptInfo.length; i++){
                bCopySheet.addCell(new Label(i, row, receiptInfo[i]));
            }
            //bCopySheet.addCell(new Label(receiptInfo.length, row, "-"));

            bCopy.write();
            bCopy.close();
            
            //update statement of account
            //add into statement of account
            
            if(!st.file.exists()){
                st.create_statement_excel();
            }
            Workbook cWorkBook = Workbook.getWorkbook(st.file);
            WritableWorkbook cCopy = Workbook.createWorkbook(st.file, cWorkBook);
            WritableSheet cCopySheet = cCopy.getSheet(0);
            
            int row2 = cCopySheet.getRows();
            int checkcount = -1;
            String closing = "";
            DecimalFormat df2 = new DecimalFormat( "0.00" );
            if(row2 != 0){
                for(int i=0; i<row2; i++){
                    String accno = cCopySheet.getCell(2, i).getContents();
                    closing = cCopySheet.getCell(0, i).getContents();
                    if(accno.equals(receiptInfo[2]) && "-".equals(closing)){
                        checkcount = i;
                    }
                }
                
                //get previos balance
                double pre_credit = 0.00;
                double pre_debit = 0.00;
                int get_row = -1;
                for(int j = 0; j<cCopySheet.getRows(); j++){
                    if(!cCopySheet.getCell(0, j).getContents().equals("-") && cCopySheet.getCell(2, j).getContents().equals(receiptInfo[2])){
                        get_row = j;
                    }else if(cCopySheet.getCell(0, j).getContents().equals("-") && cCopySheet.getCell(2, j).getContents().equals(receiptInfo[2])){
                        break;
                    }
                }
                if(get_row >=0){
                    Double balance = Double.parseDouble(cCopySheet.getCell(6, get_row).getContents());
                    if(balance > 0.00){
                        pre_credit = balance;
                    }else if(balance < 0.00){
                        String new_balance = String.valueOf(balance).replace("-", "");
                        pre_debit = Double.parseDouble(new_balance);
                    }
                }
                
                if(checkcount>=0){
                    double debit = st.get_debit(receiptInfo[2], receiptInfo[6]) + pre_debit;
                    double credit = st.get_credit(receiptInfo[2], receiptInfo[6]) + pre_credit;
                    //double credit = Double.parseDouble(cCopySheet.getCell(5, checkcount).getContents()) + pre_credit;
                    //credit = credit + Double.parseDouble(receiptInfo[3]);
                    double balance = credit - debit;
                    cCopySheet.addCell(new Label(4, checkcount, String.valueOf(df2.format(debit))));
                    cCopySheet.addCell(new Label(5, checkcount, String.valueOf(df2.format(credit))));
                    cCopySheet.addCell(new Label(6, checkcount, String.valueOf(df2.format(balance))));
                }else{
                    double debit = pre_debit;
                    double credit = st.get_credit(receiptInfo[2], receiptInfo[6]) + pre_credit;
                    //double credit = pre_credit;
                    //credit = credit + Double.parseDouble(receiptInfo[3]);
                    double balance = credit - debit;
                    cCopySheet.addCell(new Label(0, row2, "-"));
                    cCopySheet.addCell(new Label(1, row2, "-"));
                    cCopySheet.addCell(new Label(2, row2, receiptInfo[2]));
                    cCopySheet.addCell(new Label(3, row2, receiptInfo[5]));
                    cCopySheet.addCell(new Label(4, row2, String.valueOf(df2.format(debit))));
                    cCopySheet.addCell(new Label(5, row2, String.valueOf(df2.format(credit))));
                    cCopySheet.addCell(new Label(6, row2, String.valueOf(df2.format(balance))));
                    cCopySheet.addCell(new Label(7, row2, dateForm.format(date)));
                }
            }else{
                cCopySheet.addCell(new Label(0, 0, "-"));
                cCopySheet.addCell(new Label(1, 0, "-"));
                cCopySheet.addCell(new Label(2, 0, receiptInfo[2]));
                cCopySheet.addCell(new Label(3, 0, receiptInfo[5]));
                cCopySheet.addCell(new Label(4, 0, "0.00"));
                cCopySheet.addCell(new Label(5, 0, receiptInfo[3]));
                cCopySheet.addCell(new Label(6, 0, receiptInfo[3]));
                cCopySheet.addCell(new Label(7, 0, dateForm.format(date)));
            }
            
            cCopy.write();
            cCopy.close();
            
            addreceipt = true;
        }
        return addreceipt;
    }
    
    public void delete_receipt(String data)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);
        Vector<String> receiptInfo = new Vector<String>();
        int total = sheet.getRows();
        
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                for(int j =0; j<sheet.getColumns(); j++){
                    receiptInfo.add(aCopySheet.getCell(j ,i).getContents());
                }
                //break;
            }
        }    
       
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                aCopySheet.removeRow(i);
                break;
            }
        }           
        aCopy.write();
        aCopy.close();
        String[] receiptArray = receiptInfo.toArray(new String[receiptInfo.size()]);

        //st.update_credit(receiptArray);
        st.update_account(receiptArray[2], "credit");
        st.update_account(receiptArray[2], "debit");
    }
    
    public Vector view_receipt_data(){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                data.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_receipt_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Receipt No");
        headers.add("Receipt Date");
        headers.add("Account No");
        headers.add("Amount (RM)");
        headers.add("Cheque No");
        headers.add("Vehicle No");
        
        return headers;
    }
    
    public String[] load_receipt(String receiptno) throws IOException, BiffException, WriteException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        
        String[] result = new String[7];
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(receiptno)){
                for (int j = 0; j < sheet.getColumns(); j++) {
                    result[j] = sheet.getCell(j, i).getContents();
                }
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return result;
    }
    
    public String[] get_receiptNo(){
        Vector<String> data = new Vector<String>();
        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            for (int j = 0; j < sheet.getRows(); j++) {
                Cell cell = sheet.getCell(0, j);
                data.add(cell.getContents());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(data.isEmpty()){
            String[] empty={"No Receipt"};
            return empty;
        }
        String[] receiptArray = data.toArray(new String[data.size()]);
        return receiptArray;
    }
    
    public boolean edit_receipt(String[] receiptInfo) throws IOException, BiffException, WriteException{
        boolean editreceipt = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        String[] latestreceiptInfo = new String[7];
        int row = aCopySheet.getRows();
        for(int j=0; j<row; j++){
            String receiptno = aCopySheet.getCell(0, j).getContents();
            if(receiptno.equals(receiptInfo[0])){
                for(int i=0; i<receiptInfo.length; i++){
                    aCopySheet.addCell(new Label(i, j, receiptInfo[i]));
                    latestreceiptInfo[i] = receiptInfo[i];
                }
                latestreceiptInfo[6] = aCopySheet.getCell(6, j).getContents();
            }
        }
            
        aCopy.write();
        aCopy.close();
        //st.update_credit(latestreceiptInfo);
        st.update_account(latestreceiptInfo[2], "credit");
        st.update_account(latestreceiptInfo[2], "debit");
        editreceipt = true;
        
        return editreceipt;
    }
}
