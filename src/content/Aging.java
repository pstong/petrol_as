/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author PC
 */
public class Aging {
    File file=new File("aging_report.xls");
    Customer cust = new Customer();
    StatementAccount st = new StatementAccount();
    
    public void create_aging_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("aging_report.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean add_aging(String[] agingInfo)throws WriteException, IOException, BiffException{
        boolean addaging = false;
        Workbook aWorkBook = Workbook.getWorkbook(new File("customer.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("customer.xls"), aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        int isAccExist = 0;
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(agingInfo[1]) || cell.equals(agingInfo[2])){
                isAccExist++;
            }
        }        
        aCopy.write();
        aCopy.close();
        
        if(isAccExist > 0){
            Workbook bWorkBook = Workbook.getWorkbook(file);
            WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
            WritableSheet bCopySheet = bCopy.getSheet(0);
            
            int row = bCopySheet.getRows();
            Random rand = new Random();
            int r = rand.nextInt(9999999) + 1000000;
            bCopySheet.addCell(new Label(0, row, ""+r+""));
            
            for(int i=0; i<agingInfo.length; i++){
                bCopySheet.addCell(new Label(i+1, row, agingInfo[i]));
            }

            bCopy.write();
            bCopy.close();
            
            addaging = true;
        }
        return addaging;
    }
    
    public void delete_aging(String data)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                aCopySheet.removeRow(i);
                break;
            }
        }
                
        aCopy.write();
        aCopy.close();
    }
    
    public boolean edit_aging(String[] agingInfo) throws IOException, BiffException, WriteException{
        boolean editaging = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(agingInfo[0])){
                aCopySheet.addCell(new Label(1, i, agingInfo[1]));
                aCopySheet.addCell(new Label(2, i, agingInfo[2]));
                aCopySheet.addCell(new Label(3, i, agingInfo[3]));
                editaging = true;
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return editaging;
    }
    
    public Vector view_aging_data(String year, String month){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(st.file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                String[] dateString = sheet.getCell(7, j).getContents().split("/");
                String state_month = dateString[1];
                String state_year = dateString[2];
                if(year.equals(state_year) && month.equals(state_month)){
                    String[] customerInfo = cust.load_customer(sheet.getCell(2, j).getContents());
                    //display statement
                    d.add(sheet.getCell(0, j).getContents());
                    d.add(sheet.getCell(2, j).getContents());
                    d.add(customerInfo[2]);
                    d.add(customerInfo[6]);
                    d.add(customerInfo[8]);
                    d.add(sheet.getCell(4, j).getContents());
                    d.add(sheet.getCell(5, j).getContents());
                    d.add(sheet.getCell(6, j).getContents());
                    /*
                    for (int i = 0; i < sheet.getColumns(); i++) {
                        Cell cell = sheet.getCell(i, j);
                        d.add(cell.getContents());
                    }
                    */
                    d.add("\n");
                    data.add(d);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_aging_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Closing No");
        headers.add("Account No");
        headers.add("Company Name");
        headers.add("Tel No");
        headers.add("Person To Contact");
        headers.add("Debit (RM)");
        headers.add("Credit (RM)");
        headers.add("Balance (RM)");
        
        return headers;
    }
    
    public Vector loadPreviewTable(String accfrom, String closing){
        Vector data = new Vector();

        try {
            if(st.file.exists()){
                Workbook statement_book = Workbook.getWorkbook(st.file);
                Sheet statement_sheet = statement_book.getSheet(0);
                int get_row = -1;
                for(int j = 0; j<statement_sheet.getRows(); j++){
                    if(!statement_sheet.getCell(0, j).getContents().equals(closing) && statement_sheet.getCell(2, j).getContents().equals(accfrom)){
                        get_row = j;
                    }else if(statement_sheet.getCell(0, j).getContents().equals(closing) && statement_sheet.getCell(2, j).getContents().equals(accfrom)){
                        break;
                    }
                }

                if(get_row>=0){
                    Vector statement_array = new Vector();
                    Double balance = Double.parseDouble(statement_sheet.getCell(6, get_row).getContents());
                    if(balance > 0.00){
                        statement_array.add("");
                        statement_array.add("");
                        statement_array.add("TO ACCOUNT");
                        statement_array.add("BROUGHT FORWARD");
                        statement_array.add("");
                        statement_array.add("-");
                        statement_array.add(balance);
                        statement_array.add("");
                    }else if(balance < 0.00){
                        String new_balance = String.valueOf(balance).replace("-", "");
                        statement_array.add("");
                        statement_array.add("");
                        statement_array.add("TO ACCOUNT");
                        statement_array.add("BROUGHT FORWARD");
                        statement_array.add("");
                        statement_array.add(new_balance);
                        statement_array.add("-");
                        statement_array.add("");
                    }else{
                        statement_array.add("");
                        statement_array.add("");
                        statement_array.add("TO ACCOUNT");
                        statement_array.add("BROUGHT FORWARD");
                        statement_array.add("");
                        statement_array.add("");
                        statement_array.add("");
                        statement_array.add("");
                    }
                    data.add(statement_array);
                }
            }
                                
            if(new File("chit.xls").exists()){
                Workbook workbook = Workbook.getWorkbook(new File("chit.xls"));
                Sheet chit = workbook.getSheet(0);
                for (int j = 0; j < chit.getRows(); j++) {
                    if(chit.getCell(4, j).getContents().equals(accfrom) && chit.getCell(7, j).getContents().equals(closing)){
                        Vector chitArray = new Vector();
                        chitArray.add(chit.getCell(1, j).getContents());
                        chitArray.add(chit.getCell(6, j).getContents());
                        chitArray.add(chit.getCell(0, j).getContents());
                        chitArray.add("");
                        chitArray.add("");
                        chitArray.add(chit.getCell(5, j).getContents());
                        chitArray.add("");
                        chitArray.add("");

                        data.add(chitArray);
                    }
                }
            }
            
            if(new File("receipt.xls").exists()){
                Workbook bworkbook = Workbook.getWorkbook(new File("receipt.xls"));
                Sheet receipt = bworkbook.getSheet(0);
                for (int i = 0; i <receipt.getRows(); i++){
                    if(receipt.getCell(2, i).getContents().equals(accfrom) && receipt.getCell(6, i).getContents().equals(closing)){
                        Vector receiptArray = new Vector();
                        receiptArray.add(receipt.getCell(1, i).getContents());
                        receiptArray.add(receipt.getCell(5, i).getContents());
                        receiptArray.add("");
                        receiptArray.add(receipt.getCell(0, i).getContents());
                        receiptArray.add(receipt.getCell(4, i).getContents());
                        receiptArray.add("");
                        receiptArray.add(receipt.getCell(3, i).getContents());
                        receiptArray.add("");

                        data.add(receiptArray);
                    }
                }
            }
            
            String[] total = st.get_total(accfrom, closing);
            Vector totalArray = new Vector();
            totalArray.add("");
            totalArray.add("");
            totalArray.add("");
            totalArray.add("");
            totalArray.add("TOTAL (RM)");
            totalArray.add(total[0]);
            totalArray.add(total[1]);
            totalArray.add(total[2]);
            data.add(totalArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector loadPreviewTable(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Date");
        headers.add("Vehicle No");
        headers.add("Chit No");
        headers.add("Receipt No");
        headers.add("Cheque No");
        headers.add("Debit (RM)");
        headers.add("Credit (RM)");
        headers.add("Balance (RM)");
        
        return headers;
    }
}
