/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author PC
 */
public class Petrol {
    File file=new File("petrol_price.xls");
    
    public void create_petrol_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("petrol_price.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public boolean edit_petrol(String[] petrolInfo) throws IOException, BiffException, WriteException{
        boolean editpetrol = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(petrolInfo[0])){
                aCopySheet.addCell(new Label(1, i, petrolInfo[1]));
                aCopySheet.addCell(new Label(2, i, petrolInfo[2]));
                editpetrol = true;
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return editpetrol;
    }
    
    public Vector view_petrol_data() throws IOException, BiffException, WriteException{
        if(!file.exists()){
            create_petrol_excel();
            Workbook bWorkBook = Workbook.getWorkbook(file);
            WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
            WritableSheet bCopySheet = bCopy.getSheet(0);
            
            bCopySheet.addCell(new Label(0, 0, "Ron 95"));
            bCopySheet.addCell(new Label(1, 0, "UNKNOWN"));
            bCopySheet.addCell(new Label(2, 0, "UNKNOWN"));
            
            bCopySheet.addCell(new Label(0, 1, "Ron 97"));
            bCopySheet.addCell(new Label(1, 1, "UNKNOWN"));
            bCopySheet.addCell(new Label(2, 1, "UNKNOWN"));
            
            bCopySheet.addCell(new Label(0, 2, "Diesel E2"));
            bCopySheet.addCell(new Label(1, 2, "UNKNOWN"));
            bCopySheet.addCell(new Label(2, 2, "UNKNOWN"));
            
            bCopySheet.addCell(new Label(0, 3, "Diesel E5"));
            bCopySheet.addCell(new Label(1, 3, "UNKNOWN"));
            bCopySheet.addCell(new Label(2, 3, "UNKNOWN"));

            bCopy.write();
            bCopy.close();
        }
        
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                data.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_petrol_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Petrol Type");
        headers.add("Previous Price (RM)");
        headers.add("Latest Price (RM)");
        
        return headers;
    }
}
