/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;
import java.util.stream.IntStream;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author PC
 */
public class StatementAccount {

    File file = new File("statement_of_account.xls");
    Customer cust = new Customer();

    public void create_statement_excel() {
        try {
            WritableWorkbook workbook = Workbook.createWorkbook(file);
            workbook.createSheet("statement_of_account.xls", 0);

            workbook.write();
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean add_statement(String[] statementInfo) throws WriteException, IOException, BiffException {
        boolean addstatement = false;
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        int checkcount = -1;
        for (int i = 0; i < total; i++) {
            String cell = sheet.getCell(2, i).getContents();
            if (cell.equals(statementInfo[1])) {
                checkcount = i;
            }
        }
        if(checkcount >= 0){
            Random rand = new Random();
            int r = rand.nextInt(999999) + 100000;
            aCopySheet.addCell(new Label(0, checkcount, String.valueOf(r))); //closing no
            aCopySheet.addCell(new Label(1, checkcount, statementInfo[0]));  // closing date
            if ("Y".equals(statementInfo[2])) {
                aCopySheet.addCell(new Label(6, checkcount, "0.00"));
            }

            aCopy.write();
            aCopy.close();

            //update chit
            if(new File("chit.xls").exists()){
                Workbook bWorkBook = Workbook.getWorkbook(new File("chit.xls"));
                WritableWorkbook bCopy = Workbook.createWorkbook(new File("chit.xls"), bWorkBook);
                WritableSheet bCopySheet = bCopy.getSheet(0);
                Sheet bsheet = bCopy.getSheet(0);

                int btotal = bsheet.getRows();
                for (int i = 0; i < btotal; i++) {
                    String cell = bsheet.getCell(4, i).getContents();
                    String getClosing = bsheet.getCell(7, i).getContents();
                    if (cell.equals(statementInfo[1]) && getClosing.equals("-")) {
                        bCopySheet.addCell(new Label(7, i, String.valueOf(r)));
                    }
                }

                bCopy.write();
                bCopy.close();
            }

            //update receipt
            if(new File("receipt.xls").exists()){
                Workbook cWorkBook = Workbook.getWorkbook(new File("receipt.xls"));
                WritableWorkbook cCopy = Workbook.createWorkbook(new File("receipt.xls"), cWorkBook);
                WritableSheet cCopySheet = cCopy.getSheet(0);
                Sheet csheet = cCopy.getSheet(0);

                int ctotal = csheet.getRows();
                for (int i = 0; i < ctotal; i++) {
                    String cell = csheet.getCell(2, i).getContents();
                    String getClosing = csheet.getCell(6, i).getContents();
                    if (cell.equals(statementInfo[1]) && getClosing.equals("-")) {
                        cCopySheet.addCell(new Label(6, i, String.valueOf(r)));
                    }
                }

                cCopy.write();
                cCopy.close();
            }
            addstatement = true;
        }else{
            aCopy.write();
            aCopy.close();
        }
        
        return addstatement;
    }

    public void delete_statement(String data) throws WriteException, IOException, BiffException {
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for (int i = 0; i < total; i++) {
            String cell = sheet.getCell(0, i).getContents();
            if (cell.equals(data)) {
                aCopySheet.removeRow(i);
                break;
            }
        }

        aCopy.write();
        aCopy.close();
    }

    public Vector view_statement_data(String year, String month) {
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);

            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                String[] dateString = sheet.getCell(7, j).getContents().split("/");
                String state_month = dateString[1];
                String state_year = dateString[2];
                if(year.equals(state_year) && month.equals(state_month)){
                    String[] customerInfo = cust.load_customer(sheet.getCell(2, j).getContents());
                    //display statement
                    d.add(sheet.getCell(0, j).getContents());
                    d.add(sheet.getCell(1, j).getContents());
                    d.add(sheet.getCell(2, j).getContents());
                    d.add(sheet.getCell(3, j).getContents());
                    d.add(customerInfo[2]);
                    d.add(customerInfo[6]);
                    d.add(customerInfo[7]);
                    d.add(customerInfo[8]);
                    d.add(sheet.getCell(4, j).getContents());
                    d.add(sheet.getCell(5, j).getContents());
                    d.add(sheet.getCell(6, j).getContents());
                    /*
                    for (int i = 0; i < sheet.getColumns(); i++) {
                        Cell cell = sheet.getCell(i, j);
                        d.add(cell.getContents());
                    }
                     */
                    d.add("\n");
                    data.add(d);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public Vector view_statement_headers() {
        Vector headers = new Vector();

        headers.clear();
        headers.add("Closing No");
        headers.add("Closing Date");
        headers.add("Account No");
        headers.add("Vehicle No");
        headers.add("Company Name");
        headers.add("Telephone No");
        headers.add("Fax No");
        headers.add("Person to Contact");
        headers.add("Debit (RM)");
        headers.add("Credit (RM)");
        headers.add("Balance (RM)");

        return headers;
    }

    public void update_debit(String[] chitInfo) throws IOException, BiffException, WriteException {
        double debit = 0.00;
        Workbook aWorkBook = Workbook.getWorkbook(new File("chit.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("chit.xls"), aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        int total = sheet.getRows();

        for (int i = 0; i < total; i++) {
            String acc = sheet.getCell(4, i).getContents();
            String gcn = sheet.getCell(7, i).getContents();
            if (acc.equals(chitInfo[4]) && gcn.equals(chitInfo[7])) {
                debit = debit + Double.parseDouble(sheet.getCell(5, i).getContents());
            }
        }
        aCopy.write();
        aCopy.close();

        Workbook bWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
        WritableSheet bCopySheet = bCopy.getSheet(0);
        Sheet bsheet = bCopy.getSheet(0);
        int totalrow = bsheet.getRows();
        
        //get previos balance
        double pre_debit = 0.00;
        int get_row = -1;
        for(int j = 0; j<bCopySheet.getRows(); j++){
            if(bCopySheet.getCell(0, j).getContents().equals(chitInfo[7]) && bCopySheet.getCell(2, j).getContents().equals(chitInfo[4])){
                break;
            }else if(!bCopySheet.getCell(0, j).getContents().equals("-") && bCopySheet.getCell(2, j).getContents().equals(chitInfo[4])){
                get_row = j;
            }else if(bCopySheet.getCell(0, j).getContents().equals("-") && bCopySheet.getCell(2, j).getContents().equals(chitInfo[4])){
                break;
            }
        }
        if(get_row >=0){
            Double balance = Double.parseDouble(bCopySheet.getCell(6, get_row).getContents());
            if(balance < 0.00){
                String new_balance = String.valueOf(balance).replace("-", "");
                pre_debit = Double.parseDouble(new_balance);
            }
        }
        debit = debit + pre_debit;

        for (int i = 0; i < totalrow; i++) {
            String acc = bsheet.getCell(2, i).getContents();
            String gcn = bsheet.getCell(0, i).getContents();
            if (acc.equals(chitInfo[4]) && gcn.equals(chitInfo[7])) {
                bCopySheet.addCell(new Label(4, i, ""+debit+""));
                double credit = Double.parseDouble(bsheet.getCell(5, i).getContents());
                double balance = credit - debit;
                bCopySheet.addCell(new Label(6, i, ""+balance+""));
            }
        }
        
        bCopy.write();
        bCopy.close();
    }
    
    public void update_credit(String[] receiptInfo) throws IOException, BiffException, WriteException {
        double credit = 0.00;
        Workbook aWorkBook = Workbook.getWorkbook(new File("receipt.xls"));
        WritableWorkbook aCopy = Workbook.createWorkbook(new File("receipt.xls"), aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        int total = sheet.getRows();
        
        for (int i = 0; i < total; i++) {
            String acc = sheet.getCell(2, i).getContents();
            String gcn = sheet.getCell(6, i).getContents();
            if (acc.equals(receiptInfo[2]) && gcn.equals(receiptInfo[6])) {
                credit = credit + Double.parseDouble(sheet.getCell(3, i).getContents());
            }
        }
        aCopy.write();
        aCopy.close();

        Workbook bWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
        WritableSheet bCopySheet = bCopy.getSheet(0);
        Sheet bsheet = bCopy.getSheet(0);
        int totalrow = bsheet.getRows();

        //get previos balance
        double pre_credit = 0.00;
        int get_row = -1;
        for(int j = 0; j<bCopySheet.getRows(); j++){
            if(bCopySheet.getCell(0, j).getContents().equals(receiptInfo[6]) && bCopySheet.getCell(2, j).getContents().equals(receiptInfo[2])){
                break;
            }else if(!bCopySheet.getCell(0, j).getContents().equals("-") && bCopySheet.getCell(2, j).getContents().equals(receiptInfo[2])){
                get_row = j;
            }else if(bCopySheet.getCell(0, j).getContents().equals("-") && bCopySheet.getCell(2, j).getContents().equals(receiptInfo[2])){
                break;
            }
        }
        if(get_row >=0){
            Double balance = Double.parseDouble(bCopySheet.getCell(6, get_row).getContents());
            if(balance > 0.00){
                pre_credit = balance;
            }
        }
        credit = credit + pre_credit;
        
        for (int i = 0; i < totalrow; i++) {
            String acc = bsheet.getCell(2, i).getContents();
            String gcn = bsheet.getCell(0, i).getContents();
            if (acc.equals(receiptInfo[2]) && gcn.equals(receiptInfo[6])) {
                bCopySheet.addCell(new Label(5, i, ""+credit+""));
                double debit = Double.parseDouble(bsheet.getCell(4, i).getContents());
                double balance = credit - debit;
                bCopySheet.addCell(new Label(6, i, ""+balance+""));
            }
        }

        bCopy.write();
        bCopy.close();
    }
    
    public double get_debit(String accNo, String closingNo) throws IOException, BiffException, WriteException {
        double debit = 0.00;
        if(new File("chit.xls").exists()){
            Workbook aWorkBook = Workbook.getWorkbook(new File("chit.xls"));
            WritableWorkbook aCopy = Workbook.createWorkbook(new File("chit.xls"), aWorkBook);
            Sheet sheet = aCopy.getSheet(0);
            int total = sheet.getRows();

            for (int i = 0; i < total; i++) {
                String acc = sheet.getCell(4, i).getContents();
                String gcn = sheet.getCell(7, i).getContents();
                if (acc.equals(accNo) && gcn.equals(closingNo)) {
                    debit = debit + Double.parseDouble(sheet.getCell(5, i).getContents());
                }
            }
            aCopy.write();
            aCopy.close();
        }
        
        return debit;
    }

    public double get_credit(String accNo, String closingNo) throws IOException, BiffException, WriteException {
        double credit = 0.00;
        if(new File("receipt.xls").exists()){
            Workbook aWorkBook = Workbook.getWorkbook(new File("receipt.xls"));
            WritableWorkbook aCopy = Workbook.createWorkbook(new File("receipt.xls"), aWorkBook);
            Sheet sheet = aCopy.getSheet(0);
            int total = sheet.getRows();

            for (int i = 0; i < total; i++) {
                String acc = sheet.getCell(2, i).getContents();
                String gcn = sheet.getCell(6, i).getContents();
                if (acc.equals(accNo) && gcn.equals(closingNo)) {
                    credit = credit + Double.parseDouble(sheet.getCell(3, i).getContents());
                }
            }
            aCopy.write();
            aCopy.close();
        }
        
        return credit;
    }
    
    public String[] get_total(String accno, String closing){
        String[] data = new String[3];
        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);

            for (int j = 0; j < sheet.getRows(); j++) {
                if(sheet.getCell(0, j).getContents().equals(closing) && sheet.getCell(2, j).getContents().equals(accno)){
                    data[0] = sheet.getCell(4, j).getContents();
                    data[1] = sheet.getCell(5, j).getContents();
                    data[2] = sheet.getCell(6, j).getContents();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public void update_account(String acc, String type) throws IOException, BiffException, WriteException{
        Vector<String> closingNo = new Vector<String>();
        Workbook bWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook bCopy = Workbook.createWorkbook(file, bWorkBook);
        WritableSheet bCopySheet = bCopy.getSheet(0);
        Sheet bsheet = bCopy.getSheet(0);
        int totalrow = bsheet.getRows();
        
        //get all closing no from this account
        for(int i=0; i<bCopySheet.getRows(); i++){
            if(bCopySheet.getCell(2, i).getContents().equals(acc)){
                closingNo.add(bCopySheet.getCell(0, i).getContents());
            }
        }
               
        bCopy.write();
        bCopy.close();
        
        //recursive call
        String[] acc_closingNo = closingNo.toArray(new String[closingNo.size()]);
        if("debit".equals(type)){
            for(int i = 0; i < closingNo.size(); i++){
                String[] chitInfo = new String[8];
                chitInfo[0] = "";
                chitInfo[1] = "";
                chitInfo[2] = "";
                chitInfo[3] = "";
                chitInfo[4] = acc;
                chitInfo[5] = "";
                chitInfo[6] = "";
                chitInfo[7] = acc_closingNo[i];
                
                update_debit(chitInfo);
            }
        }else if("credit".equals(type)){
            for(int i = 0; i < closingNo.size(); i++){
                String[] receiptInfo = new String[7];
                receiptInfo[0] = "";
                receiptInfo[1] = "";
                receiptInfo[2] = acc;
                receiptInfo[3] = "";
                receiptInfo[4] = "";
                receiptInfo[5] = "";
                receiptInfo[6] = acc_closingNo[i];
                
                update_credit(receiptInfo);
            }
        }
    }
}
