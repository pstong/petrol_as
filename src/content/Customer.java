/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package content;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.*;
import java.awt.print.Paper;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.sql.Array;
import java.util.ArrayList;
import javax.swing.JFrame;

public class Customer {
    File file=new File("customer.xls");
    Vehicle v = new Vehicle();
    
    public void create_customer_excel(){
        try{
            WritableWorkbook workbook= Workbook.createWorkbook(file);
            workbook.createSheet("customer.xls", 0);
            
            workbook.write();
            workbook.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void add_customer(String[] customerInfo)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);

        int row = aCopySheet.getRows();
        for(int i=0; i<customerInfo.length; i++){
            aCopySheet.addCell(new Label(i, row, customerInfo[i]));
        }
        
        aCopy.write();
        aCopy.close();
    }
    
    public void delete_customer(String data)throws WriteException, IOException, BiffException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(data)){
                aCopySheet.removeRow(i);
                break;
            }
        }
                
        aCopy.write();
        aCopy.close();
    }
    
    public void edit_customer(String[] customerInfo) throws IOException, BiffException, WriteException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        WritableSheet aCopySheet = aCopy.getSheet(0);
        Sheet sheet = aCopy.getSheet(0);

        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(customerInfo[0])){
                for(int j=0; j<customerInfo.length; j++){
                    aCopySheet.addCell(new Label(j, i, customerInfo[j]));
                }
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
    }
    
    public String[] load_customer(String[] customerInfo) throws IOException, BiffException, WriteException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        
        String[] result = new String[12];
        String acc1 = customerInfo[0];
        //String acc2 = customerInfo[1];
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(acc1)){
                for (int j = 0; j < sheet.getColumns(); j++) {
                    result[j] = sheet.getCell(j, i).getContents();
                }
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return result;
    }
    
    public String[] load_customer(String Accno) throws IOException, BiffException, WriteException{
        Workbook aWorkBook = Workbook.getWorkbook(file);
        WritableWorkbook aCopy = Workbook.createWorkbook(file, aWorkBook);
        Sheet sheet = aCopy.getSheet(0);
        
        String[] result = new String[12];
        
        int total = sheet.getRows();
        for(int i=0; i<total; i++){
            String cell = sheet.getCell(0, i).getContents();
            if(cell.equals(Accno)){
                for (int j = 0; j < sheet.getColumns(); j++) {
                    result[j] = sheet.getCell(j, i).getContents();
                }
                break;
            }
        }
        
        aCopy.write();
        aCopy.close();
        
        return result;
    }
    
    public Vector view_customer_data(){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                String[] vehicleArray = v.get_vehicle(sheet.getCell(0, j).getContents());
                String vehicleString = "";
                for(int i=0; i<vehicleArray.length; i++){
                    vehicleString = vehicleString + vehicleArray[i];
                    if(i < vehicleArray.length-1){
                        vehicleString = vehicleString + ", ";
                    }
                }
                d.add(vehicleString);
                d.add("\n");
                data.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
     public Vector view_customer_data(String acc){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                if(sheet.getCell(0, j).getContents().equals(acc)){
                    for (int i = 0; i < sheet.getColumns(); i++) {
                        Cell cell = sheet.getCell(i, j);
                        d.add(cell.getContents());
                    }
                    String[] vehicleArray = v.get_vehicle(acc);
                    String vehicleString = "";
                    for(int i=0; i<vehicleArray.length; i++){
                        vehicleString = vehicleString + vehicleArray[i];
                        if(i < vehicleArray.length-1){
                            vehicleString = vehicleString + ", ";
                        }
                    }
                    d.add(vehicleString);
                    d.add("\n");
                    data.add(d);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public String[] getAccountNo(){
        String[] data = null;
        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            data = new String[sheet.getRows()];
            for (int j = 0; j < sheet.getRows(); j++) {
                Cell cell = sheet.getCell(0, j);
                data[j] = cell.getContents();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public Vector view_customer_headers(){
        Vector headers = new Vector();
           
        headers.clear();
        headers.add("Account No 1");
        headers.add("Account No 2");
        headers.add("Company Name");
        headers.add("Address 1");
        headers.add("Address 2");
        headers.add("Address 3");
        headers.add("Telephone");
        headers.add("Fax No");
        headers.add("Person To Contact");
        headers.add("Statement");
        headers.add("Service Charge %");
        headers.add("Combine Separate");
        headers.add("Vehicle No");
        
        return headers;
    }
    
    public void test(){
        Vector data = new Vector();

        try {
            Workbook workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);
            
            String line = "";
            
            data.clear();
            for (int j = 0; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int x = 0; x < sheet.getColumns(); x++) {
                    Cell cell = sheet.getCell(x, j);
                    d.add(cell.getContents());
                    line = line + cell.getContents() + "_";
                }
                line = line + "|";
                d.add("\n");
                data.add(d);
                
            }
            
            
            for (int k = 0; k < sheet.getRows(); k++) {
                String[] row=line.split("|");
                System.out.print(row[k]);
                
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
